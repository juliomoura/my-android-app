package com.example.random_app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class DisplayNameAndAgeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_name_and_age);

        Intent intent = getIntent();
        String name = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        String age  = intent.getStringExtra(MainActivity.EXTRA_MESSAGE2);

        String sentence = name + ", congratulations! You are " + age + " years old now!";

        TextView textView = findViewById(R.id.textView);
        textView.setText(sentence);

    }
    public void goToCelebrate(View view){
        Intent intent = new Intent(this, CelebrateActivity.class);
        startActivity(intent);
    }
}
