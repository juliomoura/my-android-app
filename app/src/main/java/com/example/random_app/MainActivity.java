package com.example.random_app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "com.example.randomapp.NAME";
    public static final String EXTRA_MESSAGE2 = "com.example.randomapp.AGE";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    //Testing to add 2 items in an intent
    public void sendNameandAge(View view){
        Intent intent = new Intent(this, DisplayNameAndAgeActivity.class);
        EditText editText = (EditText) findViewById(R.id.editText);
        EditText editText2 = (EditText) findViewById(R.id.editText2);
        String name = editText.getText().toString();
        String age = editText2.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, name);
        intent.putExtra(EXTRA_MESSAGE2, age);
        startActivity(intent);
    }
}
